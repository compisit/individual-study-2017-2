out = open("data.csv", "w")
# out.write("num_a,num_t,num_c,num_g,lncrna"
for i in range(60):
    out.write('{0}_a,{0}_t,{0}_c,{0}_g,'.format(i))
out.write('lncrna\n')
f_name = input("Enter lnc file name : ").strip()
cnt = 0
with open(f_name, 'r') as fin:
    for line in fin:
        line = line.strip()
        ca, ct, cc, cg = 0, 0, 0, 0
        if line[0] == '>' or len(line) != 60:
            continue
        ca += line.count('A')
        ct += line.count('T')
        cg += line.count('G')
        cc += line.count('C')
        if ca + ct + cg + cc != 60:
            continue
        for s in line:
            if s == 'A':
                out.write('1,0,0,0,')
            elif s == 'T':
                out.write('0,1,0,0,')
            elif s == 'C':
                out.write('0,0,1,0,')
            else:
                out.write('0,0,0,1,')
        out.write('1\n')
        cnt += 1
        if cnt == 20000:
            break

f_name = input("Enter pc file name : ").strip()
with open(f_name, 'r') as fin:
    for line in fin:
        line = line.strip()
        ca, ct, cc, cg = 0, 0, 0, 0
        if line[0] == '>' or len(line) != 60:
            continue
        ca += line.count('A')
        ct += line.count('T')
        cg += line.count('G')
        cc += line.count('C')
        if ca + ct + cg + cc != 60:
            continue
        for s in line:
            if s == 'A':
                out.write('1,0,0,0,')
            elif s == 'T':
                out.write('0,1,0,0,')
            elif s == 'C':
                out.write('0,0,1,0,')
            else:
                out.write('0,0,0,1,')
        out.write('0\n')
        cnt += 1
        if cnt == 40000:
            break
out.close()
