def write_table_head(file):
    ''' Generate CSV Head '''
    # 1 mer
    for a in 'ATCG':
        file.write(a + ',')
    # 2 mers
    for a in 'ATCG':
        for b in 'ATCG':
            file.write(a + b + ',')
    # 3 mers
    for a in 'ATCG':
        for b in 'ATCG':
            for c in 'ATCG':
                file.write(a + b + c + ',')
    # 4 mers
    for a in 'ATCG':
        for b in 'ATCG':
            for c in 'ATCG':
                for d in 'ATCG':
                    file.write(a + b + c + d + ',')
    # 5 mers
    for a in 'ATCG':
        for b in 'ATCG':
            for c in 'ATCG':
                for d in 'ATCG':
                    for e in 'ATCG':
                        file.write(a + b + c + d + e + ',')
    file.write('LNC\n')

def write_data(file_in, file_out, is_lnc, limit = 0):
    ''' Write Data to CSV File '''
    rna = ''
    total_count = 0
    is_start = True
    for line in file_in:
        line = line.strip()
        if line[0] != '>':
            rna += line
            continue
        if is_start:
            is_start = False
            continue
        count = [0] * 6
        data = dict()
        for i in range(1, 6):
            for j in range(len(rna) - i + 1):
                mer = rna[j:j + i]
                if mer not in data:
                    data[mer] = 0
                data[mer] += 1
                count[i] += 1
        # 1 mer
        for a in 'ATCG':
            if a in data:
                file_out.write('{0},'.format(float(data[a]) / count[2]))
            else:
                file_out.write('0,')
        # 2 mer
        for a in 'ATCG':
            for b in 'ATCG':
                mer = a + b
                if mer in data:
                    file_out.write('{0},'.format(float(data[mer]) / count[2]))
                else:
                    file_out.write('0,')
        # 3 mer
        for a in 'ATCG':
            for b in 'ATCG':
                for c in 'ATCG':
                    mer = a + b + c
                    if mer in data:
                        file_out.write('{0},'.format(float(data[mer]) / count[3]))
                    else:
                        file_out.write('0,')
        # 4 mer
        for a in 'ATCG':
            for b in 'ATCG':
                for c in 'ATCG':
                    for d in 'ATCG':
                        mer = a + b + c + d
                        if mer in data:
                            file_out.write('{0},'.format(float(data[mer]) / count[4]))
                        else:
                            file_out.write('0,')
        # 5 mer
        for a in 'ATCG':
            for b in 'ATCG':
                for c in 'ATCG':
                    for d in 'ATCG':
                        for e in 'ATCG':
                            mer = a + b + c + d + e
                            if mer in data:
                                file_out.write('{0},'.format(float(data[mer]) / count[5]))
                            else:
                                file_out.write('0,')
        file_out.write('1\n' if is_lnc else '0\n')
        if total_count % 50 == 0:
            print('Finished Parsing ', total_count)
        total_count += 1
        if limit > 0 and total_count == limit:
            break
        rna = ''

if __name__ == '__main__':
    out = open('data.csv', 'w')
    
    write_table_head(out)

    lnc_file_name = 'gencode.v27.lncRNA_transcripts.fa'
    pc_file_name = 'gencode.v27.pc_transcripts.fa'

    lnc_file = open(lnc_file_name, 'r')
    pc_file = open(pc_file_name, 'r')

    write_data(lnc_file, out, True, 20000)
    write_data(pc_file, out, False, 20000)

    lnc_file.close()
    pc_file.close()
    out.close()